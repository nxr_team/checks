from library.util import read_sheet_from_workbook
from library.util import __OLD__,__NEW__
from library.util import dataCollection_editName
from library.util import printableInXlsList, dic2list
import re
import os
import copy

SHEETNAME = 'Physical'

class Physical:
    ### ATTRIBUTE DIC ###
    DEVICE = 'Device'
    INDEX = 'index'
    INTERFACE_OLD = 'interface_OLD'
    INTERFACE_NEW = 'interface_NEW'
    NODE_NEW = 'node_NEW'
    BUNDLE_ID = 'bundle_ID'
    BUNDLE_ID_NEW = 'bundle_ID_NEW'
    LACP_MIN_LINK = 'lacp_min_link'
    DESCRIPTION = 'description'
    DESCRIPTION_NEW = 'description_NEW'
    TYPE = 'type'
    ADMIN_STATE = 'admin_state'
    STATE = 'state'
    PID = 'pid'
    TO_MIGRATE = 'to_migrate_lookUp'
    __NOTE__ = 'note'
    __CAPOMAGI__ = 'capomagi'
    ###
    TO_MIGRATE_TODAY = 'to_migrate_today'
    LOGICAL_IF = 'logicalIF'
    TOTAL_INTERFACE_OLD = 'total_interface_old'
    TOTAL_INTERFACE_NEW = 'total_interface_new'

    def __init__(self, attributeDic, newOrOld=__OLD__):
        self.classTAG = '[PHY]'
        ###
        self.attributeDic = copy.deepcopy(attributeDic)
        ###
        self.device = None
        self.interface = None
        self.bundle = None
        if newOrOld==__OLD__:
            self.device = self.attributeDic[self.DEVICE]
            self.interface = self.attributeDic[self.INTERFACE_OLD]
            bundleId = self.attributeDic.get(self.BUNDLE_ID)
            if bundleId:
                self.bundle = 'Bundle-Ether{}'.format(bundleId)
        else:
            self.device = self.attributeDic[self.NODE_NEW]
            self.interface = self.attributeDic[self.INTERFACE_NEW]
            bundleId = self.attributeDic.get(self.BUNDLE_ID_NEW)
            if bundleId:
                self.bundle = 'Bundle-Ether{}'.format(bundleId)
        ###
        self.physicalMiningDic = {self.DEVICE:self.attributeDic[self.DEVICE],
                                 self.INTERFACE_OLD:self.attributeDic[self.INTERFACE_OLD],
                                 self.NODE_NEW:self.attributeDic[self.NODE_NEW],
                                 self.INTERFACE_NEW:self.attributeDic[self.INTERFACE_NEW],
                                 self.DESCRIPTION:self.attributeDic.get(self.DESCRIPTION, ''),
                                'NEWorOLD': newOrOld} #############
        ###
        self.cmdStringTupleList = self.getCmdStringTupleList()
        #
        self.attributeDic.update({
            Physical.TOTAL_INTERFACE_OLD:[self.attributeDic.get(Physical.INTERFACE_OLD)],
            Physical.TOTAL_INTERFACE_NEW:[self.attributeDic.get(Physical.INTERFACE_NEW)]})
        #

    @staticmethod
    def getPhysicalMiningDic_SortedKeys():
        return [Physical.DEVICE, Physical.INTERFACE_OLD, Physical.NODE_NEW, Physical.INTERFACE_NEW, Physical.DESCRIPTION, 'NEWorOLD',
                'PHY STATE', 'LINE_P STATE', 'INPUT RATE', 'OUTPUT RATE',
                'TX POWER', 'RX POWER',
                'INPUT ERRORS', 'INPUT DROPS', 'OUTPUT ERRORS', 'OUTPUT DROPS'
                ]

    def getCmdStringTupleList(self):
        cmdList = []
        cmdList.append('show interfaces ' + self.interface + ' brief')
        cmdList.append('show interfaces ' + self.interface)
        cmdList.append('show controllers ' + self.interface + ' phy')
        cmdList.append('show controllers ' + self.interface + ' stats')#| i "Ingress|Egress|drop|error"')
        if self.bundle:
            cmdList.append('show interfaces ' + self.bundle)
            cmdList.append('show bundle ' + self.bundle)
            cmdList.append('show lacp ' + self.bundle)
        tmpTupleList = [(self.device, self.interface, self.classTAG, cmd) for cmd in cmdList]
        return tmpTupleList

    def getCmdStringList_fromTupleList(self):
        return [t[-1] for t in self.cmdStringTupleList]

    def getMining(self, dir):
        mining_dic = {'PHY STATE':None, 'LINE_P STATE':None, 'INPUT RATE':None, 'OUTPUT RATE':None,
                      'TX POWER': None, 'RX POWER': None,
                      'INPUT ERRORS':None, 'INPUT DROPS':None, 'OUTPUT ERRORS':None, 'OUTPUT DROPS':None}
        for cmd in self.getCmdStringList_fromTupleList():
            if cmd == 'show interfaces ' + self.interface:
                try:
                    with open(os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd))+'.txt') as f:
                        totalString = f.read()
                        phyState, lineState = re.search('^'+self.interface+' is (.*), line protocol is (.*)', totalString, re.M).groups()
                        inputRate = re.search('input rate (\S+ \S+),', totalString).group(1)
                        outputRate = re.search('output rate (\S+ \S+),', totalString).group(1)
                        mining_dic['PHY STATE'] = phyState
                        mining_dic['LINE_P STATE'] = lineState
                        mining_dic['INPUT RATE'] = inputRate
                        mining_dic['OUTPUT RATE'] = outputRate
                except:
                    print('file not found in', os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd)))
            elif 'phy' in cmd:
                try:
                    with open(os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd))+'.txt') as f:
                        totalString = f.read()
                        txPower = re.search('Tx Power:.*\((.*)\)', totalString).group(1)
                        rxPower = re.search('Rx Power:.*\((.*)\)', totalString).group(1)
                        mining_dic['TX POWER'] = txPower
                        mining_dic['RX POWER'] = rxPower
                except:
                    print('file not found in', os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd)))
            elif 'stats' in cmd:
                try:
                    with open(os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd))+'.txt') as f:
                        totalString = f.read()
                        inputErrors = sum([int(x) for x in re.findall('Input error .* (\d+)', totalString)])
                        inputDrops = sum([int(x) for x in re.findall('Input drop .* (\d+)', totalString)])
                        outputErrors = sum([int(x) for x in re.findall('Output error .* (\d+)', totalString)])
                        outputDrops = sum([int(x) for x in re.findall('Output drop .* (\d+)', totalString)])
                        mining_dic['INPUT ERRORS'] = inputErrors
                        mining_dic['INPUT DROPS'] = inputDrops
                        mining_dic['OUTPUT ERRORS'] = outputErrors
                        mining_dic['OUTPUT DROPS'] = outputDrops
                except:
                    print('file not found in', os.path.join(dir, self.device, dataCollection_editName(self.interface), dataCollection_editName(cmd)))
        #
        print(self.device, self.interface, self.classTAG, mining_dic)
        self.physicalMiningDic.update(mining_dic)
        return self.physicalMiningDic



class PhysicalSheetParse:
    def __init__(self, workbook, newOrOld):
        SHEETNAME = 'Physical'
        ### tmp ###
        #TODO: sistemare!
        if not SHEETNAME in workbook.sheetnames:
            SHEETNAME = re.search('^Physical.*Migration.*Plan', '\n'.join(workbook.sheetnames), re.M).group(0)
        ###########
        self.NEWOROLD = newOrOld
        self.dic = read_sheet_from_workbook(workbook,
                                            sheet_name=SHEETNAME,
                                            FIRSTCELL=Physical.DEVICE,
                                            keyFunction= lambda x : x.get(Physical.DEVICE) + '_' + x.get(Physical.INTERFACE_OLD))
        ##########################################################
        for k, v in self.dic.items():
            if v.get(Physical.BUNDLE_ID):
                v[Physical.LOGICAL_IF] = 'Bundle-Ether' + v[Physical.BUNDLE_ID] ##################################################################
            else:
                v[Physical.LOGICAL_IF] = v[Physical.INTERFACE_OLD]
        ##########################################################
        self.interface_to_migrate_dic = {k:v for k, v in self.dic.items() if v.get(Physical.TO_MIGRATE_TODAY)}
        self.physicalObjDic = {(attributeDic[Physical.DEVICE], attributeDic[Physical.INTERFACE_OLD]):
                                   Physical(attributeDic, newOrOld) for attributeDic in self.interface_to_migrate_dic.values()}
        ###
        for phy in self.physicalObjDic.values():
            phydic = phy.attributeDic
            if phydic.get(Physical.BUNDLE_ID):
                phyList = [p for p in self.physicalObjDic.values() if p.attributeDic.get(Physical.BUNDLE_ID, '')==phydic[Physical.BUNDLE_ID]]
                phydic[Physical.TOTAL_INTERFACE_OLD] = [p.attributeDic[Physical.INTERFACE_OLD] for p in phyList]
                phydic[Physical.TOTAL_INTERFACE_NEW] = [p.attributeDic[Physical.INTERFACE_NEW] for p in phyList]
        ###

    def getPhyInfos(self):
        deviceMappingDic = {v[Physical.DEVICE]:v[Physical.NODE_NEW] for v in self.interface_to_migrate_dic.values()}
        #logicalIFtoMigrateDic = {(v[Physical.DEVICE], v[Physical.LOGICAL_IF]):{Physical.TO_MIGRATE_TODAY:v[Physical.TO_MIGRATE_TODAY]} for v in self.interface_to_migrate_dic.values()}
        logicalIFtoMigrateDic = {(v.attributeDic[Physical.DEVICE], v.attributeDic[Physical.LOGICAL_IF]):
                                     {Physical.TO_MIGRATE_TODAY:v.attributeDic[Physical.TO_MIGRATE_TODAY],
                                      Physical.TOTAL_INTERFACE_OLD:v.attributeDic[Physical.TOTAL_INTERFACE_OLD],
                                      Physical.TOTAL_INTERFACE_NEW:v.attributeDic[Physical.TOTAL_INTERFACE_NEW]}
                                 for v in self.physicalObjDic.values()}
        return (deviceMappingDic, logicalIFtoMigrateDic)

    def getMining(self, logDir, dataString):
        dir = os.path.join(logDir, dataString)
        for physicalObj in self.physicalObjDic.values():
            physicalObj.getMining(dir)

    def dataMining(self, logDir, dataString, workbook):
        mysheet = workbook.create_sheet('PHY__' + self.NEWOROLD)#+dataString)
        keys = Physical.getPhysicalMiningDic_SortedKeys()
        mysheet.append(keys)
        self.getMining(logDir, dataString)
        for physicalObj in self.physicalObjDic.values():
            mysheet.append(
                printableInXlsList(
                    dic2list(
                        physicalObj.physicalMiningDic, keys, '')))
