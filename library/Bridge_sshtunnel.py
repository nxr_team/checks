import sshtunnel
import paramiko
import time

class TunnelBridge:
    def __init__(self, deviceName, bridgeUsr, bridgePwd, tacacsUsr, tacacsPwd, bridgeIpAddress):
        self.deviceName = deviceName.upper()
        self.bridgeUsr = bridgeUsr
        self.bridgePwd = bridgePwd
        self.tacacsUsr = tacacsUsr
        self.tacacsPwd = tacacsPwd
        self.bridgeIpAddress = bridgeIpAddress

    def sendCmdList(self, cmdList):
        return self.__sendCmdList__(['terminal len 0'] + cmdList)[1:]

    def __sendCmdList__(self, cmdStrList):
        returnStringList = []
        with sshtunnel.open_tunnel(
                (self.bridgeIpAddress, 22),
                ssh_username = self.bridgeUsr,
                ssh_password = self.bridgePwd,
                remote_bind_address=(self.deviceName, 22),
                local_bind_address=('0.0.0.0', 10022)
        ) as tunnel:
            with paramiko.SSHClient() as ssh:
                ssh.load_system_host_keys()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect('localhost',
                            port=10022,
                            username=self.tacacsUsr,
                            password=self.tacacsPwd)
                channel = ssh.invoke_shell()
                channel.settimeout(2)
                for cmd in cmdStrList:
                    print(self.deviceName, '\t-----sendCommand-----\t', repr(cmd))
                    channel.send(cmd)
                    channel.send('\n')
                    output = ''
                    try:
                        #time.sleep(1)
                        rec = channel.recv(9999).decode('ascii')
                        output = output + rec
                        rec = channel.recv(9999).decode('ascii')
                        while rec:
                            print('.', end=' ')
                            output = output + rec
                            time.sleep(0.5)
                            rec = channel.recv(9999).decode('ascii')
                    except:
                        pass
                    returnStringList.append(output.replace('\r', ''))
            channel.close()
            ssh.close()
            tunnel.close()
        return returnStringList

