import os
from openpyxl.styles import PatternFill
from library.Bridge_pyexpect import Bridge


def getCMD(deviceName, dst_dir, credentialsDic, command):
    os.makedirs(dst_dir, exist_ok=True)
    savedFileName = deviceName + dataCollection_editName(command) + '.txt'
    if savedFileName in os.listdir(dst_dir):
        with open(os.path.join(dst_dir, savedFileName)) as f:
            return f.read()
    else:
        out = Bridge(deviceName=deviceName, **credentialsDic).sendCmd(command)
        with open(os.path.join(dst_dir, savedFileName), 'w') as f:
            f.write(out)
        return out


def read_sheet_from_workbook(workbook, sheet_name, FIRSTCELL=None, keyFunction=None):
    def unmerge_sheet(sheet):
        while len(sheet.merged_cells.ranges) > 0:
            merged = sheet.merged_cells.ranges[0]
            min_col, min_row, max_col, max_row = merged.bounds
            myValue = sheet.cell(min_row, min_col).value
            sheet.unmerge_cells(merged.coord)
            for i in range(min_row, max_row + 1):
                for j in range(min_col, max_col + 1):
                    sheet.cell(i, j).value = myValue

    if not keyFunction:
        keyFunction = lambda x : x[list(x.keys())[0]]
    sheet = workbook[sheet_name]
    unmerge_sheet(sheet)

    row_len = len(tuple(sheet.rows))
    column_len = len(tuple(sheet.columns))

    firstPosition = (1,1)
    if FIRSTCELL:
        for i in range(1, row_len):
            for j in range(1, column_len):
                if sheet.cell(i, j).value == FIRSTCELL:
                    firstPosition = (i, j)
                    break
        print(FIRSTCELL, '\tfirstPosition:', firstPosition)

    keys = []
    for i in sheet.iter_rows(min_row=firstPosition[0], max_row=firstPosition[0], min_col=firstPosition[1]):
        for j in i:
            keys.append(str(j.value))

    dict = {}
    for row in sheet.iter_rows(min_row=firstPosition[0] + 1, min_col=firstPosition[1]):
        dict_int = {}
        for index, cell in enumerate(row):
            x = str(cell.value)
            if x != str(None):
                dict_int[keys[index]] = x
        try:
            keyFunction(dict_int)
        except:
            continue
        dict[keyFunction(dict_int)] = dict_int

    return dict

def dataCollection_editName(name):
    newName = name
    newName = newName.replace(' ', '_')
    newName = newName.replace('/', '_')
    newName = newName.replace(':', '_')
    newName = newName.replace('|', '_')
    return newName

def dataCollection(tunnelBridge, cmdStringList, dstDir):
    returnCmdOutput = tunnelBridge.sendCmdList(cmdStringList)
    for name, content in zip(cmdStringList, returnCmdOutput):
        print('saving - {}\t{}'.format(tunnelBridge.deviceName, name))
        file_name = os.path.join(dstDir, tunnelBridge.deviceName)
        os.makedirs(file_name, exist_ok=True)
        newName = dataCollection_editName(name)
        with open(os.path.join(file_name, newName+'.txt'), 'w') as f:
            f.write(content)

def dic2list(dic, keys, default=None):
    r = []
    for k in keys:
        r.append(dic.get(k, default))
    return r

def printableInXlsList(list):
    returnList = []
    for l in list:
        if type(l)==type([]):
            #l_ = sorted(l)
            #returnList.append('\n'.join(l_))
            l__ = [str(x) for x in l]
            returnList.append('\n'.join(l__))
        else:
            returnList.append(l)
    return returnList

### colorTable ###
GREEN = 'green'
YELLOW = 'yellow'
RED = 'red'
COLOR_PATTERN_DIC = {GREEN: PatternFill(fgColor='00FF00', fill_type='solid'),
                     YELLOW : PatternFill(fgColor='FFFF00', fill_type='solid'),
                     RED : PatternFill(fgColor='FF0000', fill_type='solid'),}


### newOrOld ###
__OLD__ = 'OLD'
__NEW__ = 'NEW'