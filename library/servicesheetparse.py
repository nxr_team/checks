from library.util import read_sheet_from_workbook
from library.util import dataCollection_editName
from library.util import printableInXlsList, dic2list
from library.util import GREEN, YELLOW, RED, COLOR_PATTERN_DIC
from library.util import __OLD__,__NEW__
from openpyxl.styles import PatternFill
from library.physicalsheetparse import Physical
from library.staticroutessheetparse import StaticRoutesParse
import re
import copy
import os


SHEETNAME = 'Services'

class Service:
    ### ATTRIBUTE DIC ###
    DEVICE = 'Device'
    DEVICE_NEW = 'Device_new'
    INDEX = 'index'
    TO_MIGRATE = 'to_migrate'
    INTERFACE_NAME = 'interface_name'
    INTERFACE_NAME_NEW = 'interface_name_new'
    INTERFACE_DESCRIPTION = 'interface_description'
    INTERFACE_DESCRIPTION_NEW = 'interface_description_new'
    INTERFACE_STATUS = 'interface_status'
    INTERFACE_VRF = 'interface_vrf'
    INTERFACE_IP_ADDRESS = 'interface_ip_address'
    BRIDGE_GROUP_NAME = 'bridge_group_name'
    BRIDGE_DOMAIN_NAME = 'bridge_domain_name'
    VFI_NAME = 'vfi_name'
    XCONNECT_GROUP_NAME = 'xconnect_group_name'
    XCONNECT_P2P_NAME = 'xconnect_p2p_name'
    L2_NEIGHBOR = 'l2_neighbor'
    BVI_LEN = 'bvi_len'
    BVI_NAME = 'bvi_name'
    # BVI_IP_ADDRESS = 'bvi_ip_address'
    HSRP_INTERFACE = 'hsrp_interface'
    HSRP_VIP = 'hsrp_vip'
    HSRP_PRIORITY = 'hsrp_priority'
    OSPF_PROCESS = 'ospf_process'
    OSPF_AREA = 'ospf_area'
    OSPF_PASSIVE = 'ospf_passive'
    BGP_NEIGHBOR_IP = 'bgp_neighbor_ip'
    BGP_REMOTE_AS = 'bgp_remote_as'
    BGP_NEIGHBOR_INTERFACE = 'bgp_neighbor_interface'
    STATIC_ROUTES = 'static_routes'
    BFD = 'bfd'
    SCENARIO = 'scenario'
    NOTE = 'note'
    ### ### ### ###
    TO_MIGRATE_TODAY = 'to_migrate_today'
    TO_MIGRATE_TODAY_PHY_TAG = 'to_migrate_phy_tag'
    GROUP = 'group'
    ### ### ### ###
    STATIC_ROUTES_PARSE_OBJ = 'static_routes_parse_obj'
    SHOW_CMDS_BRIEF = 'Show Commands Brief'
    ROUTING_PROTOCOL_SHUT = 'Shut Dynamic Routing Protocol'
    ROUTING_PROTOCOL_NOSHUT = 'No Shut Dynamic Routing Protocol'
    L1_INTERFACE_SHUT = 'L1 Interface Shut'
    L1_INTERFACE_NOSHUT = 'L1 Interface NoShut'
    L2_INTERFACE_SHUT = 'L2 Interface Shut'
    L2_INTERFACE_NOSHUT = 'L2 Interface NoShut'
    L3_INTERFACE_SHUT = 'L3 Interface Shut'
    L3_INTERFACE_NOSHUT = 'L3 Interface NoShut'

    @staticmethod
    def getKeys():
        return [
            Service.DEVICE,
            Service.INDEX,
            Service.INTERFACE_NAME,
            Service.INTERFACE_NAME_NEW,
            Service.INTERFACE_DESCRIPTION,
            Service.INTERFACE_DESCRIPTION_NEW,
            Service.TO_MIGRATE,
            Service.INTERFACE_STATUS,
            Service.SCENARIO,
            Service.NOTE,
            Service.INTERFACE_VRF,
            Service.INTERFACE_IP_ADDRESS,
            Service.BRIDGE_GROUP_NAME,
            Service.BRIDGE_DOMAIN_NAME,
            Service.VFI_NAME,
            Service.XCONNECT_GROUP_NAME,
            Service.XCONNECT_P2P_NAME,
            Service.L2_NEIGHBOR,
            # Service.BVI_LEN,
            Service.BVI_NAME,
            Service.HSRP_INTERFACE,
            Service.HSRP_VIP,
            Service.HSRP_PRIORITY,
            Service.OSPF_PROCESS,
            Service.OSPF_AREA,
            Service.OSPF_PASSIVE,
            Service.BGP_NEIGHBOR_IP,
            Service.BGP_REMOTE_AS,
            Service.BGP_NEIGHBOR_INTERFACE,
            Service.STATIC_ROUTES,
            Service.BFD,
        ]

    def __init__(self, attributeDic, deviceMappingDic, newOrOld=__OLD__, physicalToMigrateTodayTAG='YES', totalPhyOld=[], totalPhyNew=[], staticRoutesParse=None):
        ###
        self.attributeDic = copy.deepcopy(attributeDic)
        #
        self.attributeDic[Service.STATIC_ROUTES_PARSE_OBJ] = staticRoutesParse
        #
        self.attributeDic[Service.DEVICE_NEW] = deviceMappingDic[self.attributeDic[Service.DEVICE]]
        self.attributeDic[Service.TO_MIGRATE_TODAY_PHY_TAG] = physicalToMigrateTodayTAG
        if not self.attributeDic.get(Service.INTERFACE_NAME_NEW):
            self.attributeDic[Service.INTERFACE_NAME_NEW] = self.attributeDic[Service.INTERFACE_NAME]
        self.__classObjList__ = Service.getClassObjList()
        self.serviceMiningDic = {Service.DEVICE:self.attributeDic[Service.DEVICE],
                                 Service.INDEX:self.attributeDic[Service.INDEX],
                                 #Service.INTERFACE_NAME:self.attributeDic[Service.INTERFACE_NAME],
                                 Service.INTERFACE_DESCRIPTION:self.attributeDic.get(Service.INTERFACE_DESCRIPTION,''),
                                 Service.SCENARIO:self.attributeDic[Service.SCENARIO],
                                 'NEWorOLD':newOrOld}  ##########
        if newOrOld == __OLD__:
            self.serviceMiningDic.update({Service.INTERFACE_NAME:self.attributeDic[Service.INTERFACE_NAME]})
        else:
            self.serviceMiningDic.update({Service.INTERFACE_NAME: self.attributeDic.get(Service.INTERFACE_NAME_NEW,
                                                                                        self.attributeDic[Service.INTERFACE_NAME])})
        self.serviceMiningDic_sortedKeys = Service.getServiceMiningDic_SortedKeys()
        tmp = self.__getL2L3InterfaceMigrationCMDs__(totalPhyOld, totalPhyNew)
        self.cmdDic = {Service.ROUTING_PROTOCOL_SHUT:[],
                       Service.ROUTING_PROTOCOL_NOSHUT:[],
                       Service.SHOW_CMDS_BRIEF:[],
                       Service.L1_INTERFACE_SHUT: tmp[0],
                       Service.L1_INTERFACE_NOSHUT: tmp[1],
                       Service.L2_INTERFACE_SHUT: tmp[2],
                       Service.L2_INTERFACE_NOSHUT: tmp[3],
                       Service.L3_INTERFACE_SHUT: tmp[4],
                       Service.L3_INTERFACE_NOSHUT: tmp[5]
                       }
        ###
        ## CREATE OBJECT DICTIONARY ##
        self.objDic = {}
        self.cmdStringTupleList = []
        for objclass in self.__classObjList__:
            myobj = objclass(self.attributeDic, newOrOld)
            myobj_tag = myobj.classTAG
            if myobj.cmdStringList:
                self.cmdStringTupleList = self.cmdStringTupleList + myobj.getCmdStringTupleList()
            self.objDic[myobj_tag] = myobj
            tmp = myobj.getMigrationCMDs()
            self.cmdDic[Service.ROUTING_PROTOCOL_SHUT] = self.cmdDic[Service.ROUTING_PROTOCOL_SHUT] + tmp[0]
            self.cmdDic[Service.ROUTING_PROTOCOL_NOSHUT] = self.cmdDic[Service.ROUTING_PROTOCOL_NOSHUT] + tmp[1]
            self.cmdDic[Service.SHOW_CMDS_BRIEF] = self.cmdDic[Service.SHOW_CMDS_BRIEF] + tmp[2]

    def getMining(self, dir):
        for obj in self.objDic.values():
            self.serviceMiningDic.update(obj.getMining(dir))
        return self.serviceMiningDic

    def cmp(self, other):
        returnDic = {}
        for myobj, otherobj in zip(self.objDic.values(), other.objDic.values()):
            returnDic.update(myobj.cmp(otherobj))
        return returnDic

    @staticmethod
    def getClassObjList():
        return [extra_check, general_check, hsrp_check, bgp_check, ospf_check, bfd_check, bridgeGroup_check, xconnect_check]

    @staticmethod
    def getServiceMiningDic_SortedKeys():
        return [Service.DEVICE, Service.INDEX, Service.INTERFACE_NAME, Service.INTERFACE_DESCRIPTION, Service.SCENARIO, 'NEWorOLD'] + \
               sum([objC.getMiningDicSortedKeys() for objC in Service.getClassObjList()], [])

    @staticmethod
    def getServiceMiningDic_SortedKeys_bridgeLimited_split():
        return [[Service.DEVICE, Service.INDEX, Service.INTERFACE_NAME, Service.DEVICE_NEW, Service.INTERFACE_NAME_NEW, Service.INTERFACE_DESCRIPTION, Service.SCENARIO],
               bridgeGroup_check.getMiningDicSortedKeys()[1:]]

    def __getL2L3InterfaceMigrationCMDs__(self, totalPhyOld, totalPhyNew):
        L2_OLD = []
        L2_NEW = []
        L3_OLD = []
        L3_NEW = []
        if 'L3' in self.attributeDic[Service.SCENARIO]:
            L3_OLD = 'interface ' + self.attributeDic[Service.INTERFACE_NAME] + ' shutdown'
            L3_NEW = 'no interface ' + self.attributeDic[Service.INTERFACE_NAME_NEW] + ' shutdown'
        elif 'L2' in self.attributeDic[Service.SCENARIO]:
            if self.attributeDic.get(Service.BVI_NAME):
                L3_OLD = 'interface ' + self.attributeDic[Service.BVI_NAME] + ' shutdown'
                L3_NEW = 'no interface ' + self.attributeDic[Service.BVI_NAME] + ' shutdown'
                L2_OLD = 'interface ' + self.attributeDic[Service.INTERFACE_NAME] + ' shutdown'
                L2_NEW = 'no interface ' + self.attributeDic[Service.INTERFACE_NAME_NEW] + ' shutdown'
            else:
                L2_OLD = 'interface ' + self.attributeDic[Service.INTERFACE_NAME] + ' shutdown'
                L2_NEW = 'no interface ' + self.attributeDic[Service.INTERFACE_NAME_NEW] + ' shutdown'
        else:
            L2_OLD = '?'
            L2_NEW = '?'
            L3_OLD = '?'
            L3_NEW = '?'
        #
        L1_OLD = ['interface {} shutdown'.format(i) for i in totalPhyOld]
        L1_NEW = ['no interface {} shutdown'.format(i) for i in totalPhyNew]
        if 'Bundle-Ether' in self.attributeDic[Service.INTERFACE_NAME]:
            L1_OLD.insert(0, 'interface {} shutdown'.format(self.attributeDic[Service.INTERFACE_NAME].split('.')[0]))
            L1_NEW.insert(0, 'no interface {} shutdown'.format(self.attributeDic[Service.INTERFACE_NAME_NEW].split('.')[0]))
        #
        return L1_OLD, L1_NEW, L2_OLD, L2_NEW, L3_OLD, L3_NEW


    @staticmethod
    def getAll_Keys():
        return [
            Service.DEVICE,
            Service.INDEX,
            Service.GROUP,
            Service.TO_MIGRATE_TODAY_PHY_TAG,
            Service.INTERFACE_NAME,
            Service.INTERFACE_NAME_NEW,
            Service.INTERFACE_DESCRIPTION,
            Service.INTERFACE_DESCRIPTION_NEW,
            Service.SCENARIO,
            Service.NOTE,
            Service.INTERFACE_VRF,
            Service.INTERFACE_IP_ADDRESS,
            ] + general_check.getMiningDicSortedKeys() + [
            Service.BVI_NAME,
            Service.BRIDGE_GROUP_NAME,
            Service.BRIDGE_DOMAIN_NAME,
            ] + bridgeGroup_check.getMiningDicSortedKeys() + [
            Service.XCONNECT_GROUP_NAME,
            Service.XCONNECT_P2P_NAME,
            Service.L2_NEIGHBOR,
            ] + xconnect_check.getMiningDicSortedKeys() + [
            Service.HSRP_INTERFACE,
            Service.HSRP_VIP,
            Service.HSRP_PRIORITY,
            ] + hsrp_check.getMiningDicSortedKeys() + [
            Service.OSPF_PROCESS,
            Service.OSPF_AREA,
            Service.OSPF_PASSIVE,
            ] + ospf_check.getMiningDicSortedKeys() + [
            Service.BGP_NEIGHBOR_IP,
            Service.BGP_REMOTE_AS,
            Service.BGP_NEIGHBOR_INTERFACE,
            ] + bgp_check.getMiningDicSortedKeys() + [
            Service.STATIC_ROUTES,
            Service.BFD ,
            ] + bfd_check.getMiningDicSortedKeys() + [
            #
            Service.SHOW_CMDS_BRIEF,
            Service.ROUTING_PROTOCOL_SHUT,
            Service.L3_INTERFACE_SHUT,
            Service.L2_INTERFACE_SHUT,
            #Service.L1_INTERFACE_SHUT,     #danno su MI05
            #Service.L1_INTERFACE_NOSHUT,   #danno su MI05
            Service.L2_INTERFACE_NOSHUT,
            Service.L3_INTERFACE_NOSHUT,
            Service.ROUTING_PROTOCOL_NOSHUT
            ]

    @staticmethod
    def getAll_Keys__color():
        bridgeCFG = PatternFill(fgColor='FABF8F', fill_type='solid')
        bridgeSTATUS = PatternFill(fgColor='FDE9D9', fill_type='solid')
        xconnectCFG = PatternFill(fgColor='92CDDC', fill_type='solid')
        xconnectSTATUS = PatternFill(fgColor='DAEEF3', fill_type='solid')
        hsrpCFG = PatternFill(fgColor='B1A0C7', fill_type='solid')
        hsrpSTATUS = PatternFill(fgColor='E4DFEC', fill_type='solid')
        ospfCFG = PatternFill(fgColor='C4D79B', fill_type='solid')
        ospfSTATUS = PatternFill(fgColor='EBF1DE', fill_type='solid')
        bgpCFG = PatternFill(fgColor='DA9694', fill_type='solid')
        bgpSTATUS = PatternFill(fgColor='F2DCDB', fill_type='solid')
        dynPro = PatternFill(fgColor='DDD9C4', fill_type='solid')
        l3 = PatternFill(fgColor='C5D9F1', fill_type='solid')
        l2 = PatternFill(fgColor='F2DCDB', fill_type='solid')
        l1 = PatternFill(fgColor='F2AC2B', fill_type='solid')
        bfdCFG = PatternFill(fgColor='FABF8F', fill_type='solid')
        bfdSTATUS = PatternFill(fgColor='FDE9D9', fill_type='solid')
        return [None for x in range(15)] + \
               [bridgeCFG for x in range(3)] + \
               [bridgeSTATUS for x in range(6)] + \
               [xconnectCFG for x in range(3)] + \
               [xconnectSTATUS for x in range(2)] + \
               [hsrpCFG for x in range(3)] + \
               [hsrpSTATUS for x in range(2)] + \
               [ospfCFG for x in range(3)] + \
               [ospfSTATUS for x in range(5)] + \
               [bgpCFG for x in range(3)] + \
               [bgpSTATUS for x in range(4)] + \
               [None for x in range(1)] + \
               [bfdCFG for x in range(1)] + \
               [bfdSTATUS for x in range(2)] + \
               [None for x in range(1)] + \
               [dynPro for x in range(1)] + \
               [l3 for x in range(1)] + \
               [l2 for x in range(1)] + \
               [l1 for x in range(0)]+\
               [l2 for x in range(1)] + \
               [l3 for x in range(1)] + \
               [dynPro for x in range(1)]

    def getAll(self):
        returnDic = {}
        returnDic.update(self.attributeDic)
        returnDic.update(self.serviceMiningDic)
        returnDic.update(self.cmdDic)
        return returnDic



class mother_check:
    def __init__(self, attributeDic, newOrOld):
        if newOrOld==__OLD__:
            self.device = attributeDic.get(Service.DEVICE)
            self.logicalIf = attributeDic.get(Service.INTERFACE_NAME)
            self.logicalIf_other = attributeDic.get(Service.INTERFACE_NAME_NEW, attributeDic[Service.INTERFACE_NAME])
        else:
            self.device = attributeDic.get(Service.DEVICE_NEW)
            self.logicalIf = attributeDic.get(Service.INTERFACE_NAME_NEW, attributeDic[Service.INTERFACE_NAME])
            self.logicalIf_other = attributeDic.get(Service.INTERFACE_NAME)
        self.staticRoutesParseObj = attributeDic.get(Service.STATIC_ROUTES_PARSE_OBJ)
        self.vrf_name = attributeDic.get(Service.INTERFACE_VRF)
        self.l3interface = attributeDic.get(Service.BVI_NAME, '')
        self.ip = attributeDic.get(Service.INTERFACE_IP_ADDRESS, '')
        if not self.l3interface:
            self.l3interface = self.logicalIf
            self.l3interface_other = self.logicalIf_other
        else:
            self.l3interface_other = self.l3interface
        ###
        self.classTAG = '[#]'
        self.cmdStringList = ['@']
        self.miningDic = {}

    def getCmdStringTuple(self, cmdString):
        return (self.device, self.logicalIf, self.classTAG, cmdString)
    def getDir(self, dir, commandName_edited):
        return os.path.join(dir, self.device, dataCollection_editName(self.logicalIf), commandName_edited)
    def getCmdStringTupleList(self):
        tmpTupleList = []
        for cmdString in self.cmdStringList:
            tmpTupleList.append(self.getCmdStringTuple(cmdString))
        return tmpTupleList

    @staticmethod
    def getMiningDicSortedKeys():
        return []

    def getMigrationCMDs(self):
        return [[],[], []]

class extra_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        ###
        self.classTAG = '[EXTRA]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        if self.vrf_name:
            vrftag = ' vrf ' + self.vrf_name
        else:
            vrftag = ''
        cmdList.append('show bgp' + vrftag + ' scale')
        return cmdList

    def getMining(self, dir):
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return []

    def cmp(self, other):
        returnDic = {}
        return returnDic

class general_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        ###
        self.classTAG = '[GENERAL]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        if self.vrf_name:
            vrftag = ' vrf ' + self.vrf_name
        else:
            vrftag = ''
        cmdList.append('show arp' + vrftag + ' | i ' + self.l3interface)
        cmdList.append('show bgp' + vrftag + ' scale')
        cmdList.append('show interfaces ' + self.logicalIf)
        bundle = re.search('Bundle-Ether\d+', self.logicalIf)
        if bundle:
            bundle = bundle.group(0)
            cmdList.append('show interfaces ' + bundle)
            cmdList.append('show bundle ' + bundle)
            cmdList.append('show lacp ' + bundle)
        if self.l3interface != self.logicalIf:
            cmdList.append('show interfaces ' + self.l3interface)
        return cmdList

    def getMining(self, dir):
        arp_dict = {}
        logicalIfStatus = ''
        l3IfStatus = ''
        for commandName in self.cmdStringList:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir, commandName_edited)) as f:
                    totalString = f.read()
                    if 'arp' in commandName:
                        tmp = re.findall('^(\S+)[ ]+\S+[ ]+(\S+)[ ]+Dynamic', totalString, re.M)
                        for t in tmp:
                            ip, mac = t
                            arp_dict[ip] = mac
                    elif self.logicalIf in commandName:
                        x = re.search('^' + self.logicalIf + ' is .*, line protocol is (.*)', totalString, re.M)
                        if x:
                            logicalIfStatus = x.group(1)
                    elif self.l3interface in commandName:
                        x = re.search('^' + self.l3interface + ' is .*, line protocol is (.*)', totalString, re.M)
                        if x:
                            l3IfStatus = x.group(1)
            except:
                print('file not found in', super().getDir(dir, commandName_edited))
        if not l3IfStatus:
            l3IfStatus = logicalIfStatus
        if arp_dict:
            #
            self.miningDic['DYNAMIC ARP IP'] = []
            self.miningDic['DYNAMIC ARP MAC'] = []
            #
            for ip in sorted(list(arp_dict.keys())):
                self.miningDic['DYNAMIC ARP IP'].append(ip)
                self.miningDic['DYNAMIC ARP MAC'].append(arp_dict[ip])
        self.miningDic['IF STATUS'] = logicalIfStatus + '_' + l3IfStatus
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['IF STATUS','DYNAMIC ARP IP', 'DYNAMIC ARP MAC']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        #
        oldDic = {}
        for ip,mac in zip(self.miningDic.get('DYNAMIC ARP IP', ''),
                          self.miningDic.get('DYNAMIC ARP MAC', '')):
            oldDic[ip]=mac
        newDic = {}
        for ip, mac in zip(other.miningDic.get('DYNAMIC ARP IP', ''),
                           other.miningDic.get('DYNAMIC ARP MAC', '')):
            newDic[ip] = mac
        #
        if len(oldDic)!=len(newDic):
            returnDic['DYNAMIC ARP IP'] = YELLOW
            returnDic['DYNAMIC ARP MAC'] = YELLOW
        if self.miningDic.get('IF STATUS', '') != other.miningDic.get('IF STATUS', ''):
            returnDic['IF STATUS'] = RED
        return returnDic

    def getMigrationCMDs(self):
        returnListDeleteOnOld, returnListCreateOnNew = self.staticRoutesParseObj.get(self.device, self.l3interface)
        return [returnListDeleteOnOld, returnListCreateOnNew, []]

class hsrp_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.hsrp_interface_list = attributeDic.get(Service.HSRP_INTERFACE, '').splitlines()
        self.hsrp_vip_list = attributeDic.get(Service.HSRP_VIP, '').splitlines()
        self.hsrp_priority_list = attributeDic.get(Service.HSRP_PRIORITY, '').splitlines()
        for index in range(len(self.hsrp_interface_list)):
            self.hsrp_interface_list[index] = self.l3interface
        ###
        self.classTAG = '[HSRP]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        for hsrp_interf in self.hsrp_interface_list:
            cmdList.append('show hsrp ' + hsrp_interf + ' detail')
        return cmdList

    def getMining(self, dir):
        hsrp_dic = {hsrp_interface:{'STATUS':None} for hsrp_interface in self.hsrp_interface_list}
        for commandName in self.cmdStringList:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir, commandName_edited)) as f:
                    totalString = f.read()
                    hsrp_interface = re.search('show hsrp (\S+)', commandName).group(1)
                    hsrp_status = re.search('Local state is (\S+),', totalString).group(1)
                    hsrp_dic[hsrp_interface]['STATUS'] = hsrp_status
            except:
                print('file not found in', super().getDir(dir, commandName_edited))
        if self.hsrp_interface_list:
            #
            self.miningDic['HSRP INTERFACE'] = []
            self.miningDic['HSRP STATUS'] = []
            #
            for hsrp_interface in sorted(self.hsrp_interface_list):
                self.miningDic['HSRP INTERFACE'].append(hsrp_interface)
                self.miningDic['HSRP STATUS'].append(hsrp_dic[hsrp_interface]['STATUS'])
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['HSRP INTERFACE', 'HSRP STATUS']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('HSRP STATUS', '') != other.miningDic.get('HSRP STATUS', ''):
            returnDic['HSRP STATUS'] = RED
        return returnDic

    def getMigrationCMDs(self):
        cmdShowList = []
        for vip in self.hsrp_vip_list:
            if ':' in vip:
                ipv_tag = 'ipv6'
            else:
                ipv_tag = 'ipv4'
            cmdShowList.append('show hsrp ' + ipv_tag + ' brief | i "Interface|'+vip+'( |$)"')
        return [[], [], cmdShowList]

class bgp_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.bgp_neighbor_ip_list = attributeDic.get(Service.BGP_NEIGHBOR_IP, '').splitlines()
        self.bgp_neighbor_interface_list = attributeDic.get(Service.BGP_NEIGHBOR_INTERFACE, '').splitlines()
        for index in range(len(self.bgp_neighbor_interface_list)):
            bgp_interf = self.bgp_neighbor_interface_list[index]
            self.bgp_neighbor_interface_list[index] = self.l3interface
        ###
        self.classTAG = '[BGP]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        for index in range(len(self.bgp_neighbor_ip_list)):
            if ':' in self.bgp_neighbor_ip_list[index]:
                ipv6tag = ' ipv6 unicast'
            else:
                ipv6tag = ''
            if self.vrf_name:
                vrftag = ' vrf ' + self.vrf_name
            else:
                vrftag = ''
            cmdList.append('show bgp' + vrftag + ipv6tag + ' summary')
            cmdList.append('show bgp' + vrftag + ipv6tag + ' neighbors ' + self.bgp_neighbor_ip_list[index])
            cmdList.append('show bgp' + vrftag + ipv6tag + ' neighbors ' + self.bgp_neighbor_ip_list[index] + ' routes')
            #cmdList.append('show bgp' + vrftag + ipv6tag + ' neighbors ' + self.bgp_neighbor_ip_list[index] + ' advertised-routes')
            cmdList.append('show bgp' + vrftag + ipv6tag + ' neighbors ' + self.bgp_neighbor_ip_list[index] + ' advertised-count')
        return cmdList

    def getMining(self, dir):
        bgp_neighbor_ip_dic = {bgp_neighbor_ip:{'STATUS':None, 'RECEIVED':None, 'ADVERTISED':None}for bgp_neighbor_ip in self.bgp_neighbor_ip_list}
        for commandName in [cmdName for cmdName in self.cmdStringList if 'summary' in cmdName]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir, commandName_edited)) as f:
                    totalString = f.read().splitlines()
                    neighborStartString = re.search('^Neighbor.*', '\n'.join(totalString), re.M).group(0)
                    neighborStartString_index = totalString.index(neighborStartString)
                    neighbors_string = '\n'.join(totalString[neighborStartString_index+1:-2])
                    tmp = re.findall('^(\S+)\s+.*\S+[ ]+(\S+)', neighbors_string, re.M)
                    for x in tmp:
                        ip, nprefix = x
                        if re.search('\d+', nprefix):
                            status = 'Established'
                        else:
                            status = nprefix
                            nprefix = '\\'
                        if ip in bgp_neighbor_ip_dic:
                            bgp_neighbor_ip_dic[ip]['STATUS'] = status
                            bgp_neighbor_ip_dic[ip]['RECEIVED'] = nprefix
            except:
                print('file not found in', super().getDir(dir, commandName_edited))
        for commandName in self.cmdStringList:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            if 'advertised-count' in commandName:
                try:
                    with open(super().getDir(dir, commandName_edited)) as f:
                        totalString = f.read()
                        bgp_neighbor_ip = re.search('neighbors (\S+)', commandName).group(1)
                        if bgp_neighbor_ip_dic[bgp_neighbor_ip]['STATUS'] == 'Established':
                            bgp_advertised_routes = re.search('Advertised: (\d+)', totalString)
                            if bgp_advertised_routes:
                                bgp_advertised_routes = bgp_advertised_routes.group(1)
                            else:
                                bgp_advertised_routes = '0'
                            bgp_neighbor_ip_dic[bgp_neighbor_ip]['ADVERTISED'] = bgp_advertised_routes
                        else:
                            if 'advertised' in commandName:
                                bgp_neighbor_ip_dic[bgp_neighbor_ip]['ADVERTISED'] = '\\'
                            else:
                                bgp_neighbor_ip_dic[bgp_neighbor_ip]['RECEIVED'] = '\\'
                except:
                    print('file not found in', super().getDir(dir, commandName_edited))
        if self.bgp_neighbor_ip_list:
            #
            self.miningDic['BGP NEIGHBOR IP'] = []
            self.miningDic['BGP NEIGHBOR STATUS'] = []
            self.miningDic['BGP RECEIVED'] = []
            self.miningDic['BGP ADVERTISED'] = []
            #
            for bgp_neighbor_ip in sorted(self.bgp_neighbor_ip_list):
                self.miningDic['BGP NEIGHBOR IP'].append(bgp_neighbor_ip)
                self.miningDic['BGP NEIGHBOR STATUS'].append(bgp_neighbor_ip_dic[bgp_neighbor_ip]['STATUS'])
                self.miningDic['BGP RECEIVED'].append(bgp_neighbor_ip_dic[bgp_neighbor_ip]['RECEIVED'])
                self.miningDic['BGP ADVERTISED'].append(bgp_neighbor_ip_dic[bgp_neighbor_ip]['ADVERTISED'])
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['BGP NEIGHBOR IP', 'BGP NEIGHBOR STATUS', 'BGP RECEIVED', 'BGP ADVERTISED']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('BGP NEIGHBOR STATUS', '') != other.miningDic.get('BGP NEIGHBOR STATUS', ''):
            returnDic['BGP NEIGHBOR STATUS'] = RED
        if self.miningDic.get('BGP RECEIVED', '') and other.miningDic.get('BGP RECEIVED', ''):
            self_bgp_received = sum([int(x) for x in self.miningDic.get('BGP RECEIVED', ['0']) if re.search('^\d+$', x, re.M)])
            other_bgp_received = sum([int(x) for x in other.miningDic.get('BGP RECEIVED', ['0']) if re.search('^\d+$', x, re.M)])
            if not ((0.95*self_bgp_received) <= other_bgp_received <= (1.05*self_bgp_received)):
                returnDic['BGP RECEIVED'] = YELLOW
        if self.miningDic.get('BGP ADVERTISED', '') != other.miningDic.get('BGP ADVERTISED', ''):
            returnDic['BGP ADVERTISED'] = RED
        return returnDic

    def getMigrationCMDs(self):
        cmdListShut = []
        cmdListNoShut = []
        cmdShowList = []
        for neighIp in self.bgp_neighbor_ip_list:
            if self.vrf_name:
                vrftag = ' vrf ' + self.vrf_name
            else:
                vrftag = ''
            cmdListShut.append('router bgp 30722' + vrftag + ' neighbor ' + neighIp + ' shutdown')
            cmdListNoShut.append('no router bgp 30722' + vrftag + ' neighbor ' + neighIp + ' shutdown')
            if ':' in neighIp:
                ipv_tag = 'ipv6 unicast'
            else:
                ipv_tag = 'ipv4 unicast'
            cmdShowList.append('show bgp' + vrftag + ' ' + ipv_tag + ' summary | i "BGP VRF|Neighbor|'+neighIp+'"')
        return [cmdListShut, cmdListNoShut, cmdShowList]

class ospf_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.ospf_process_list = attributeDic.get(Service.OSPF_PROCESS, '').splitlines()
        self.ospf_isPassive_list = attributeDic.get(Service.OSPF_PASSIVE, '').splitlines()
        self.ospf_area_list = attributeDic.get(Service.OSPF_AREA, '').splitlines()
        ###
        self.classTAG = '[OSPF]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        for index in range(len(self.ospf_process_list)):
            if self.vrf_name:
                vrftag = ' vrf ' + self.vrf_name
            else:
                vrftag = ''
            cmdList.append('show ospf ' + self.ospf_process_list[index] + vrftag + ' interface brief')
            cmdList.append('show ospf ' + self.ospf_process_list[index] + vrftag + ' interface ' + self.l3interface)
            cmdList.append('show ospf ' + self.ospf_process_list[index] + vrftag + ' neighbor ' + self.l3interface)
            cmdList.append('show ospf ' + self.ospf_process_list[index] + vrftag + ' neighbor ' + self.l3interface + ' detail')
            cmdList.append('show route' + vrftag + ' | i ' + self.l3interface )
        return cmdList

    def getMining(self, dir):
        ospf_dic = {self.l3interface: {'INTERFACE STATE': None, 'INTERFACE NBRS': None, 'NEIGHBORS IP': {}}}
        brief_commandName = [cmd for cmd in self.cmdStringList if 'interface brief' in cmd]
        if brief_commandName:
            brief_commandName = brief_commandName[0]
            try:
                with open(super().getDir(dir, dataCollection_editName(brief_commandName)+'.txt')) as f:
                    totalString = f.read()
                    l3coordinate = re.search('[\d./]+', self.l3interface).group(0)
                    tmp = re.search('^[A-Z]+' + l3coordinate.replace('.', '\.') + '[ ]+\S+[ ]+\S+[ ]+\S+[ ]+\S+[ ]+(\S+)[ ]+(\S+)', totalString, re.M)
                    if tmp:
                        interface_state, interface_nbrs = tmp.groups()
                        ospf_dic[self.l3interface]['INTERFACE STATE'] = interface_state
                        ospf_dic[self.l3interface]['INTERFACE NBRS'] = interface_nbrs
                        brief_commandName = [cmd for cmd in self.cmdStringList if 'neighbor' in cmd and not 'detail' in cmd][0]
                        if interface_nbrs != '0/0':
                            with open(super().getDir(dir,dataCollection_editName(brief_commandName))+'.txt') as f:
                                totalString = f.read()
                                for line in re.findall('.*' + self.l3interface, totalString):
                                    if 'neighbor' not in line:
                                        state, neighbor_ip = re.search('^\S+\s+\d+\s+([A-Z]+).*\s(\S+)\s+'+self.l3interface+'$', line).groups()
                                        ospf_dic[self.l3interface]['NEIGHBORS IP'][neighbor_ip] = {'STATE': state}
            except:
                print('file not found in', super().getDir(dir,dataCollection_editName(brief_commandName))+'.txt')
            #
            self.miningDic['OSPF INTERFACE'] = [self.l3interface]
            self.miningDic['OSPF INTERFACE STATE'] = [ospf_dic[self.l3interface]['INTERFACE STATE']]
            self.miningDic['OSPF INTERFACE NBRS'] = [ospf_dic[self.l3interface]['INTERFACE NBRS']]
            self.miningDic['OSPF NEIGHBORS IP'] = []
            self.miningDic['OSPF NEIGHBORS STATE'] = []
            #
            for neighbor_ip in sorted(ospf_dic[self.l3interface]['NEIGHBORS IP'].keys()):
                self.miningDic['OSPF NEIGHBORS IP'].append(neighbor_ip)
                self.miningDic['OSPF NEIGHBORS STATE'].append(ospf_dic[self.l3interface]['NEIGHBORS IP'][neighbor_ip]['STATE'])
            ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['OSPF INTERFACE','OSPF INTERFACE STATE','OSPF INTERFACE NBRS','OSPF NEIGHBORS IP','OSPF NEIGHBORS STATE']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('OSPF INTERFACE STATE', '') != other.miningDic.get('OSPF INTERFACE STATE', ''):
            returnDic['OSPF INTERFACE STATE'] = YELLOW
        if self.miningDic.get('OSPF INTERFACE NBRS', '') != other.miningDic.get('OSPF INTERFACE NBRS', ''):
            returnDic['OSPF INTERFACE NBRS'] = RED
        if self.miningDic.get('OSPF NEIGHBORS IP', '') != other.miningDic.get('OSPF NEIGHBORS IP', ''):
            returnDic['OSPF NEIGHBORS IP'] = RED
        if self.miningDic.get('OSPF NEIGHBORS STATE', '') != other.miningDic.get('OSPF NEIGHBORS STATE', ''):
            returnDic['OSPF NEIGHBORS STATE'] = RED
        return returnDic

    def getMigrationCMDs(self):
        cmdListShut = []
        cmdListNoShut = []
        cmdShowList = []
        for index in range(len(self.ospf_process_list)):
            if self.vrf_name:
                vrftag = ' vrf ' + self.vrf_name
            else:
                vrftag = ''
            ospfProcess = self.ospf_process_list[index]
            ospfArea = self.ospf_area_list[index]
            if self.ospf_isPassive_list:
                ospfIsPassive = self.ospf_isPassive_list[index]
            else:
                ospfIsPassive = False
            if not ospfIsPassive:
                cmdListShut.append('router ospf ' + ospfProcess + vrftag + ' area ' + ospfArea + ' interface ' + self.l3interface + ' passive enable')
                cmdListNoShut.append('no router ospf ' + ospfProcess + vrftag + ' area ' + ospfArea + ' interface ' + self.l3interface_other + ' passive enable')
            for ip in self.ip.splitlines():
                cmdShowList.append('show ospf ' + ospfProcess + vrftag + ' interface brief | i "Interface |' + ip + '"')
        return [cmdListShut, cmdListNoShut, cmdShowList]

class bfd_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.isBfd_list = attributeDic.get(Service.BFD, '').splitlines()
        self.bgp_neighbor_ip = attributeDic.get(Service.BGP_NEIGHBOR_IP, '').splitlines()
        ###
        self.classTAG = '[BFD]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        if self.isBfd_list:
            for index in range(len(self.isBfd_list)):
                cmdList.append('show bfd session interface ' + self.l3interface)
                cmdList.append('show bfd session interface ' + self.l3interface + ' detail')
            for ip in self.ip.splitlines():
                ip = ip.split('/')[0]
                cmdList.append('show bfd session source ' + ip)
                cmdList.append('show bfd session source ' + ip + ' detail')
        return cmdList

    def getMining(self, dir):
        bfd_dic = {}
        for commandName in [cmd for cmd in self.cmdStringList if 'detail' not in cmd]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir,commandName_edited)) as f:
                    totalString = f.read()
                    ### session on interfaces ###
                    l3coordinate = re.search('[\d./]+', self.l3interface).group(0)
                    tmp = re.search('(^\S+'+l3coordinate.replace('.', '\.')+')\s+.*\s+(\S+)\s+$', totalString, re.M)
                    if tmp:
                        src_interface, bfd_status = tmp.groups()
                        bfd_dic[src_interface] = bfd_status
                    ### session on bgp ###
                    for ip in self.bgp_neighbor_ip:
                        tmp = re.search('(^\S+)\s+' + ip.replace('.', '\.') + '.*\s+.*\s+(\S+)\s+$', totalString, re.M)
                        if tmp:
                            src_ip, bfd_status = tmp.groups()
                            bfd_dic[src_ip] = bfd_status
            except:
                print('file not found in', super().getDir(dir,commandName_edited))
        if bfd_dic:
            #
            self.miningDic['BFD SCR'] = []
            self.miningDic['BFD STATUS'] = []
            #
            for bfd_src in sorted(list(bfd_dic.keys())):
                self.miningDic['BFD SCR'].append(bfd_src)
                self.miningDic['BFD STATUS'].append(bfd_dic[bfd_src])
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['BFD SCR', 'BFD STATUS']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('BFD STATUS', '') != other.miningDic.get('BFD STATUS', ''):
            returnDic['BFD STATUS'] = RED
        return returnDic

class bridgeGroup_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.bridge_group_name_list = attributeDic.get(Service.BRIDGE_GROUP_NAME, '').splitlines()
        self.bridge_domain_name_list = attributeDic.get(Service.BRIDGE_DOMAIN_NAME, '').splitlines()
        ###
        self.classTAG = '[BRIDGE]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        for index in range(len(self.bridge_group_name_list)):
            cmdList.append('show l2vpn bridge-domain bd-name ' + self.bridge_domain_name_list[index])
            # for i in range(8):
            for i in range(1):
                cmdList.append('show l2vpn forwarding bridge-domain ' + self.bridge_group_name_list[index] + ':' +
                               self.bridge_domain_name_list[index] + ' mac-address location 0/{}/CPU0'.format(i))
            if 'BVI' in self.l3interface:
                for i in range(10):
                    cmdList.append('show uidb data location 0/{}/CPU0 {} egress | i UIDB_EXID_BVI_SIMPLIFIED'.format(i, self.l3interface))
        return cmdList

    def getMining(self, dir):
        bviSimplified_bit = 0
        for commandName in [cmd for cmd in self.cmdStringList if 'uidb' in cmd]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir, commandName_edited)) as f:
                    totalString = f.read()
                    bviSimplified_bit = bviSimplified_bit + \
                                               sum([int(i) for i in re.findall('UIDB_EXID_BVI_SIMPLIFIED\s+(\d+)', totalString, re.M)])
            except:
                print('file not found in', super().getDir(dir, commandName_edited))
        if bviSimplified_bit:
            bviSimplified_bit = 1
        ###
        dynamic_mac_learnt = []
        for commandName in [cmd for cmd in self.cmdStringList if 'mac-address location' in cmd]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir,commandName_edited)) as f:
                    totalString = f.read()
                    dynamic_mac_learnt = dynamic_mac_learnt + re.findall('^(\S+)[ ]+dynamic', totalString, re.M)
            except:
                print('file not found in', super().getDir(dir,commandName_edited))
        ###
        accessPWs_dic = {}
        vfi_dic = {}
        for commandName in [cmd for cmd in self.cmdStringList if 'bd-name' in cmd]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir,commandName_edited)) as f:
                    totalString = f.read()
                    __tmpStruct__ = totalString.split('List of ')
                    tmpStruct = {x.splitlines()[0]:x for x in __tmpStruct__}
                    accessPW_string = tmpStruct['Access PWs:']
                    for x in re.findall('Neighbor (\S+) pw-id (\S+), state: (\S+),', accessPW_string):
                        accessPWs_dic[x[0]+'_'+x[1]]=x[2]
                    vfi_string = tmpStruct['VFIs:']
                    for x in re.findall('Neighbor (\S+) pw-id (\S+), state: (\S+),', vfi_string):
                        vfi_dic[x[0]+'_'+x[1]]=x[2]

            except:
                print('file not found in', super().getDir(dir,commandName_edited))
        ###
        self.miningDic['UIDB_EXID_BVI_SIMPLIFIED bit'] = bviSimplified_bit
        if dynamic_mac_learnt:
            self.miningDic['BRIDGE DYNAMIC MAC'] = sorted(list(set(dynamic_mac_learnt)))
        if accessPWs_dic:
            self.miningDic['BRIDGE ACCESS-PW NEIGHBOR'] = []
            self.miningDic['BRIDGE ACCESS-PW STATUS'] = []
            for k in sorted(list(accessPWs_dic.keys())):
                self.miningDic['BRIDGE ACCESS-PW NEIGHBOR'].append(k)
                self.miningDic['BRIDGE ACCESS-PW STATUS'].append(accessPWs_dic[k])
        if vfi_dic:
            self.miningDic['BRIDGE VFI NEIGHBOR'] = []
            self.miningDic['BRIDGE VFI STATUS'] = []
            for k in sorted(list(vfi_dic.keys())):
                self.miningDic['BRIDGE VFI NEIGHBOR'].append(k)
                self.miningDic['BRIDGE VFI STATUS'].append(vfi_dic[k])
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['UIDB_EXID_BVI_SIMPLIFIED bit', 'BRIDGE DYNAMIC MAC', 'BRIDGE ACCESS-PW NEIGHBOR', 'BRIDGE ACCESS-PW STATUS', 'BRIDGE VFI NEIGHBOR', 'BRIDGE VFI STATUS']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('BRIDGE DYNAMIC MAC', '') != other.miningDic.get('BRIDGE DYNAMIC MAC', ''):
            returnDic['BRIDGE DYNAMIC MAC'] = YELLOW
        if other.miningDic.get('UIDB_EXID_BVI_SIMPLIFIED bit', 0) != 0:
            returnDic['UIDB_EXID_BVI_SIMPLIFIED bit'] = RED
        return returnDic

    def getMigrationCMDs(self):
        cmdShowList = []
        for index in range(len(self.bridge_group_name_list)):
            cmdShowList.append('show l2vpn bridge-domain bd-name ' + self.bridge_domain_name_list[index] + ' | i "\\\\),"')
        return [[],[],cmdShowList]

class xconnect_check(mother_check):
    def __init__(self, attributeDic, newOrOld):
        super().__init__(attributeDic, newOrOld)
        ###
        self.xconnect_group_name_list = attributeDic.get(Service.XCONNECT_GROUP_NAME, '').splitlines()
        self.xconnect_p2p_name_list = attributeDic.get(Service.XCONNECT_P2P_NAME, '').splitlines()
        self.xconnect_p2p_neighbor_list = attributeDic.get(Service.L2_NEIGHBOR, '').splitlines()
        ###
        self.classTAG = '[XCONN]'
        self.cmdStringList = self.getCMDList_to_Collect()
        self.miningDic = {}

    def getCMDList_to_Collect(self):
        cmdList = []
        for index in range(len(self.xconnect_group_name_list)):
            cmdList.append('show l2vpn xconnect group ' + self.xconnect_group_name_list[index] + ' xc-name ' +
                           self.xconnect_p2p_name_list[index])
            cmdList.append('show l2vpn xconnect group ' + self.xconnect_group_name_list[index] + ' xc-name ' +
                           self.xconnect_p2p_name_list[index] + ' detail')
        return cmdList

    def getMining(self, dir):
        self.miningDic['XCONN STATUS'] = []
        self.miningDic['XCONN DESCRIPTION'] = []
        for commandName in [cmd for cmd in self.cmdStringList if 'detail' not in cmd]:
            commandName_edited = dataCollection_editName(commandName) + '.txt'
            try:
                with open(super().getDir(dir,commandName_edited)) as f:
                    totalString = f.read()
                    #
                    logicalIf = re.search('[\d/.]+', self.logicalIf).group(0).replace('.', '\.')
                    tmp = re.search('^[ ]+(\S+)\s+(\S+{})\s+(\S+)'.format(logicalIf), totalString, re.M)
                    if self.xconnect_p2p_neighbor_list:
                        tmp = re.search('^[ ]+(\S+)\s+(\S+{})\s+(\S+)'.format(logicalIf), totalString, re.M)
                        xconn_status, seg1_description, seg1_status = tmp.groups()
                        for neiIp in self.xconnect_p2p_neighbor_list:
                            neiIp = neiIp.split()[0]
                            tmp = re.search('({})\s+\d+\s+(\S+)'.format(neiIp.replace('.', '\.')), totalString)
                            seg2_description, seg2_status = tmp.groups()
                            self.miningDic['XCONN STATUS'].append(
                                '{}__{}_{}'.format(xconn_status, seg1_status, seg2_status))
                            self.miningDic['XCONN DESCRIPTION'].append(
                                '{}_{}'.format(seg1_description, seg2_description))
                    else:
                        tmp = re.search('^[ ]+(.*)\s+\-', totalString, re.M)
                        if tmp:
                            tmp = tmp.group(1)
                            tmp = tmp.split()
                            if len(tmp) == 6:
                                tmp = tmp[:4] + tmp[5:]
                            xconn_status, seg1_description, seg1_status, seg2_description, seg2_status = tmp
                            self.miningDic['XCONN STATUS'].append(
                                '{}__{}_{}'.format(xconn_status, seg1_status, seg2_status))
                            self.miningDic['XCONN DESCRIPTION'].append(
                                '{}_{}'.format(seg1_description, seg2_description))
                    #
            except:
                print('file not found in', super().getDir(dir,commandName_edited))
        ##
        print(self.device, self.l3interface, self.classTAG, self.miningDic)
        ##
        return self.miningDic

    @staticmethod
    def getMiningDicSortedKeys():
        return ['XCONN STATUS', 'XCONN DESCRIPTION']

    def cmp(self, other): #TODO: it's too stupid!
        returnDic = {}
        if self.miningDic.get('XCONN STATUS', '') != other.miningDic.get('XCONN STATUS', ''):
            returnDic['XCONN STATUS'] = RED
        return returnDic

    def getMigrationCMDs(self):
        cmdShowList = []
        for index in range(len(self.xconnect_group_name_list)):
            cmdShowList.append('show l2vpn xconnect group ' + self.xconnect_group_name_list[index] + ' xc-name ' +
                           self.xconnect_p2p_name_list[index] + ' detail | i state')
        return [[], [], cmdShowList]

########################################################################################################################
########################################################################################################################

class ServiceSheetParse:
    def __init__(self, workbook, deviceMappingDic, logicalIFtoMigrateDic, newOrOld):
        SHEETNAME = 'Services'
        self.NEWOROLD = newOrOld
        tmpdic = read_sheet_from_workbook(workbook,
                                            sheet_name=SHEETNAME, FIRSTCELL = Service.DEVICE,
                                            keyFunction=lambda x : x.get(Service.DEVICE) + '_' + x.get(Service.INTERFACE_NAME))
        staticRoutesParse = StaticRoutesParse(workbook)
        self.serviceObjDic = {(attributeDic[Service.DEVICE], int(attributeDic[Service.INDEX]), attributeDic[Service.INTERFACE_NAME]):
                                   Service(attributeDic, deviceMappingDic, newOrOld,
                                           logicalIFtoMigrateDic.get((attributeDic[Service.DEVICE], attributeDic[Service.INTERFACE_NAME].split('.')[0]), {}).get(Physical.TO_MIGRATE_TODAY, 'YES1'),
                                           logicalIFtoMigrateDic.get((attributeDic[Service.DEVICE], attributeDic[Service.INTERFACE_NAME].split('.')[0]), {}).get(Physical.TOTAL_INTERFACE_OLD, []),
                                           logicalIFtoMigrateDic.get((attributeDic[Service.DEVICE], attributeDic[Service.INTERFACE_NAME].split('.')[0]), {}).get(Physical.TOTAL_INTERFACE_NEW, []),
                                           staticRoutesParse
                                           )
                              for attributeDic in tmpdic.values()
                              if (attributeDic[Service.DEVICE], attributeDic[Service.INTERFACE_NAME].split('.')[0]) in logicalIFtoMigrateDic
                              and attributeDic.get(Service.TO_MIGRATE_TODAY)}
        #
        tmpList = [(v[Service.DEVICE], v[Service.INDEX]) for v in tmpdic.values()]
        self.__setGroupValue__(tmpList)

    def __setGroupValue__(self, tmpList):
        for service in self.serviceObjDic.values():
            k = (service.attributeDic[Service.DEVICE], service.attributeDic[Service.INDEX])
            if tmpList.count(k) > 1:
                service.attributeDic[Service.GROUP] = 'TRUE'
            else:
                service.attributeDic[Service.GROUP] = 'FALSE'

    def getMining(self, logDir, dataString):
        dir = os.path.join(logDir, dataString)
        for serviceObj in self.serviceObjDic.values():
            serviceObj.getMining(dir)



    def dataMining(self, logDir, dataString, workbook):
        mysheet = workbook.create_sheet('SERV__' + self.NEWOROLD)#+dataString)
        keys = Service.getServiceMiningDic_SortedKeys()
        mysheet.append(keys)
        self.getMining(logDir, dataString)
        for serviceObj in self.serviceObjDic.values():
            mysheet.append(
                printableInXlsList(
                    dic2list(
                        #serviceObj.getMining(dir), keys, '')))
                        serviceObj.serviceMiningDic, keys, '')))

    def dataAll(self, logDir, dataString, workbook):
        mysheet = workbook.create_sheet('SwC__' + self.NEWOROLD)#+dataString)
        keys = Service.getAll_Keys()
        mysheet.append(keys)
        for index, color in enumerate(Service.getAll_Keys__color()):
            if color:
                mysheet.cell(row=1, column=1+index).fill = color
        self.getMining(logDir, dataString)
        for serviceObj in self.serviceObjDic.values():
            mysheet.append(
                printableInXlsList(
                    dic2list(
                        serviceObj.getAll(), keys, '')))
        ###
        #for rowIndex, row in enumerate(mysheet["2:" + str(len(tuple(mysheet.rows)))], 2):
        #    column = keys.index(Service.GROUP)+1
        #    columnIndex = chr(ord('A') + keys.index(Service.INDEX))
        #    formula = '=OR(EXACT({columnIndex}{myRow};{columnIndex}{rowBefore});EXACT({columnIndex}{myRow};{columnIndex}{rowAfter}))'\
        #        .format(columnIndex=columnIndex, myRow=rowIndex, rowBefore=rowIndex-1, rowAfter=rowIndex+1)
        #    formula = formula.replace(';', ',')
        #    mysheet.cell(column=column, row=rowIndex, value=formula)
        ###
        color1 = PatternFill(fgColor='DAEEF3', fill_type='solid')
        color2 = PatternFill(fgColor='FDE9D9', fill_type='solid')
        last_index = '0'
        last_color = color2
        for cells in mysheet["2:" + str(len(tuple(mysheet.rows)))]:
            new_index = cells[Service.getAll_Keys().index(Service.INDEX)].value
            if new_index == last_index:
                color = last_color
            else:
                last_index = new_index
                if last_color == color1:
                    color = color2
                    last_color = color2
                else:
                    color = color1
                    last_color = color1
            for cell in cells:
                cell.fill = color


    def cmp(self, otherParse, workbook):
        color1 = PatternFill(fgColor='DAEEF3', fill_type='solid')
        color2 = PatternFill(fgColor='FDE9D9', fill_type='solid')
        #
        mysheet = workbook.create_sheet('cmp')
        keys = Service.getServiceMiningDic_SortedKeys()
        mysheet.append(keys)
        for row_index, serviceKey in enumerate(sorted(list(self.serviceObjDic.keys())), start=1):
            selfServiceObj = self.serviceObjDic[serviceKey]
            otherServiceObj = otherParse.serviceObjDic[serviceKey]
            selfDataDic = selfServiceObj.serviceMiningDic
            otherDataDic = otherServiceObj.serviceMiningDic
            colorDic = selfServiceObj.cmp(otherServiceObj)
            #
            selfDataDic_listCell = printableInXlsList(dic2list(selfDataDic, keys, ''))
            otherDataDic_listCell = printableInXlsList(dic2list(otherDataDic, keys, ''))
            colorDic_listCell = printableInXlsList(dic2list(colorDic, keys, ''))
            #
            for col_index, selfData, otherData, color in zip(range(len(selfDataDic_listCell)), selfDataDic_listCell, otherDataDic_listCell, colorDic_listCell):
                mysheet.cell(row=(2*row_index), column=col_index+1).value = selfData
                mysheet.cell(row=(2*row_index)+1, column=col_index+1).value = otherData
                if color:
                    fill = COLOR_PATTERN_DIC.get(color)
                else:
                    if row_index%2==0:
                        fill = color1
                    else:
                        fill = color2
                mysheet.cell(row=(2*row_index), column=col_index + 1).fill = fill
                mysheet.cell(row=(2*row_index)+1, column=col_index+1).fill = fill
            #
            if RED in colorDic_listCell:
                globalColor = RED
            elif YELLOW in colorDic_listCell:
                globalColor = YELLOW
            else:
                globalColor = GREEN
            if globalColor:
                mysheet.cell(row=(2 * row_index), column=keys.index(Service.INDEX) + 1).fill = COLOR_PATTERN_DIC.get(globalColor)
                mysheet.cell(row=(2 * row_index) + 1, column=keys.index(Service.INDEX) + 1).fill = COLOR_PATTERN_DIC.get(globalColor)
            #

    def readBGPvrfScale(self, logDir):
        def getKeys():
            return['DEVICE',
                   'VRF',
                   'NEIGHBORS_CONFIGURED',
                   'NEIGHBORS_ESTABLISHED',
                   'IPV4_PREFIXES',
                   'IPV4_PATHS',
                   'IPV4_IMPORTED_PATHS',
                   'IPV6_PREFIXES',
                   'IPV6_PATHS',
                   'IPV6_IMPORTED_PATHS',
                   'TOTAL_PREFIXES',
                   'TOTAL_PATHS']

        s = ('{:15.15}|'*len(getKeys())).format(*getKeys())
        print(s)
        print(('-'*15 + '|')*len(getKeys()))
        #
        serviceTupleDic = {(t[0],t[-1]):t
                           for servObj in self.serviceObjDic.values()
                           for t in servObj.cmdStringTupleList
                           if 'scale' in t[-1]}
        returnDic = {}
        for tupl in serviceTupleDic.values():
            device, logicalIf, classTAG, cmdString = tupl
            print('-MIB', device)
            vrf = re.search('vrf (\S+)', cmdString)
            if vrf:
                vrf = vrf.group(1)
            else:
                continue
            with open(os.path.join(logDir, device, dataCollection_editName(logicalIf), dataCollection_editName(cmdString)+'.txt')) as f:
                deviceDic = returnDic.get(device)
                if not deviceDic:
                    print('-MIB create new deviceDic: {}'.format(device))
                    deviceDic = {}
                    returnDic[device] = deviceDic
                s = f.read()
                neighbors_configured = 0
                neighbors_established = 0
                ipv4_prefixes = 0
                ipv4_paths = 0
                ipv4_imported_paths = 0
                ipv6_prefixes = 0
                ipv6_paths = 0
                ipv6_imported_paths = 0
                total_prefixes = 0
                total_paths = 0
                #
                neighbors_configured, neighbors_established = re.search('Neighbors Configured: (\d+)\s+Established: (\d+)', s, re.M).groups()
                tmp = re.search('IPv4 Unicast\s+(\d+)\s+(\d+)', s, re.M)
                if tmp:
                    ipv4_prefixes, ipv4_paths = tmp.groups()
                tmp = re.search('IPv4 Unicast\s+\d+\s+\d+.*\s.*Imported\s+(\d+)', s, re.M)
                if tmp:
                    ipv4_imported_paths = tmp.group(1)
                tmp = re.search('IPv6 Unicast\s+(\d+)\s+(\d+)', s, re.M)
                if tmp:
                    ipv6_prefixes, ipv6_paths = tmp.groups()
                tmp = re.search('IPv6 Unicast\s+\d+\s+\d+.*\s.*Imported\s+(\d+)', s, re.M)
                if tmp:
                    ipv6_imported_paths = tmp.group(1)
                x = re.search('Total\s+(\d+)\s+(\d+)', s, re.M)
                if x:
                    total_prefixes, total_paths = x.groups()
                deviceDic[vrf] = {'DEVICE':device,
                                  'VRF':vrf,
                                  'NEIGHBORS_CONFIGURED':neighbors_configured,
                                  'NEIGHBORS_ESTABLISHED':neighbors_established,
                                  'IPV4_PREFIXES':ipv4_prefixes,
                                  'IPV4_PATHS':ipv4_paths,
                                  'IPV4_IMPORTED_PATHS':ipv4_imported_paths,
                                  'IPV6_PREFIXES': ipv6_prefixes,
                                  'IPV6_PATHS': ipv6_paths,
                                  'IPV6_IMPORTED_PATHS': ipv6_imported_paths,
                                  'TOTAL_PREFIXES':total_prefixes,
                                  'TOTAL_PATHS':total_paths
                                  }
                l = [str(x) for x in dic2list(deviceDic[vrf], getKeys(), [])]
                s = ('{:15.15}|' * len(l)).format(*l)
                print(s)

        import json
        print(json.dumps(returnDic, indent=4))
        return getKeys(), returnDic



