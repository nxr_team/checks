from library.util import read_sheet_from_workbook
import re

DEVICE_OLD = 'DeviceOld'
STATIC_ROUTE = 'StaticRoute'
INTERFACE_OLD = 'InterfaceOld'

class StaticRoutesParse:
    def __init__(self, workbook):
        SHEETNAME = 'StaticRoutes'
        try:
            self.dic = read_sheet_from_workbook(workbook,
                                                sheet_name=SHEETNAME, FIRSTCELL=DEVICE_OLD,
                                                keyFunction=lambda x: tuple([x.get(DEVICE_OLD), x.get(INTERFACE_OLD, x.get(STATIC_ROUTE))]))
        except:
            self.dic = {}

    def get(self, deviceOld, interfaceOld):
        print('######################', deviceOld, interfaceOld, self.dic.keys())
        returnListDeleteOnOld = []
        returnListCreateOnNew = []
        staticRoutesList = [s[STATIC_ROUTE] for s in self.dic.values() if s[DEVICE_OLD] == deviceOld and s[INTERFACE_OLD] == interfaceOld]
        for staticRoute in staticRoutesList:
            returnListDeleteOnOld.append('no ' + staticRoute)
            returnListCreateOnNew.append(staticRoute)
        print('######################', returnListDeleteOnOld, returnListCreateOnNew)
        return returnListDeleteOnOld, returnListCreateOnNew
