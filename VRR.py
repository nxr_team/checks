import os, datetime, json, re
from library.util import getCMD
from openpyxl import Workbook


class DeviceRR:
    def __init__(self, deviceName, logDir, credentialsDic):
        print(deviceName)
        command = 'show bgp all all summary'
        fileName = deviceName + '_' + command.strip().replace(' ', '_') + '.txt'
        os.makedirs(logDir, exist_ok=True)
        showCmd = getCMD(deviceName, logDir, credentialsDic, command)
        self.d = {'name':deviceName, 'results':{}}
        self.parse(showCmd)
        print(len(self.d['results'].keys()))

    def parse(self, showCmd):
        afs = showCmd.split('Address Family: ')
        for af in afs[1:]:
            af_name = af.splitlines()[0]
            lines = re.findall('^\d.*', af, re.M)
            lines = [re.search('^(\S+) .* (\S+)$', line).groups() for line in lines]
            for line in lines:
                self.d['results'][(af_name, line[0])] = line[1]



def __main__():
    LOG_DIR = 'LOG VRR'

    now = datetime.datetime.now()
    now_str = '{}_{}_{}__{}_{}'.format(now.year, now.month, now.day, now.hour, now.minute)
    # now_str = '{}_{}_{}'.format(now.year, now.month, now.day)

    with open(os.path.join('credentials.json')) as f:
        credentialsDic = json.loads(f.read())

    wb = Workbook()
    ws = wb.active

    deviceList = input('VRR names separated by space: ').split()
    deviceList = sorted(list(set([i.strip() for i in deviceList])))
    print(deviceList)

    ws.append(['AF', 'Neighbor', *deviceList] + ['check'])
    deviceObjList = [DeviceRR(deviceName, logDir=os.path.join(LOG_DIR, now_str), credentialsDic=credentialsDic) for deviceName in deviceList]
    keys = sum([list(device.d['results'].keys()) for device in deviceObjList], [])
    keys = sorted(list(set(keys)))

    for k in keys:
        result = [device.d['results'].get(k, '###') for device in deviceObjList]
        ws.append([str(i) for i in list(k)] + result + [str(len(set(result)) == 1)])

    wb.save(os.path.join(LOG_DIR, now_str, 'VRR__{}.xlsx'.format(now_str)))

if __name__ == '__main__':
    __main__()

