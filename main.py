import sys
sys.path.insert(0, 'library')
#
import library.physicalsheetparse as phy
import library.servicesheetparse as serv
import library.Bridge_pyexpect as bridge
from library.util import dataCollection, dataCollection_editName
from library.util import printableInXlsList, dic2list
from library.util import __OLD__, __NEW__
import openpyxl
import os
import re
import datetime
import json
import copy

def getContextDic(workbook, __newOrOld__, partialDstDir, credentials, fileDir):
    physicalParse = phy.PhysicalSheetParse(workbook, newOrOld=__newOrOld__)
    serviceParse = serv.ServiceSheetParse(workbook, *physicalParse.getPhyInfos(), newOrOld=__newOrOld__)
    contexDic = {'physicalParse':physicalParse,
                 'serviceParse':serviceParse,
                 'newOrOld':__newOrOld__,
                 'dataString':None,
                 'workbook':workbook,
                 'partialDstDir':partialDstDir,
                 'credentials':credentials,
                 'fileDir':fileDir}
    return contexDic

def runDataCollection(contextDic, classTagList=[]):
    physicalParse = contextDic['physicalParse']
    serviceParse = contextDic['serviceParse']
    ########################################### DATA COLLECTION ###########################################
    #
    serviceTupleListList = [servObj.cmdStringTupleList for servObj in serviceParse.serviceObjDic.values()]
    physicalTupleListList = [phyObj.cmdStringTupleList for phyObj in physicalParse.physicalObjDic.values()]
    cmdTupleList = sorted(list(set(sum(serviceTupleListList+physicalTupleListList, []))))
    if classTagList:
        cmdTupleList = [cmdTuple for cmdTuple in cmdTupleList if cmdTuple[2] in classTagList]
    #
    for index, i in enumerate(cmdTupleList):
        print('{} - {}\t{}\t{}\t{}'.format(index, *i))
    print('###len:', len(cmdTupleList))
    #
    cmdTupleList_bgp_routes = []    #[t for t in cmdTupleList if ' routes' in t[-1]]
    cmdTupleList = [t for t in cmdTupleList if t not in cmdTupleList_bgp_routes]     #if ' routes' not in t[-1]]
    #
    deviceList = sorted(list(set([t[0] for t in cmdTupleList])))
    if not contextDic['dataString']:
        data = datetime.datetime.now()
        dataString = '{}_{}_{}__{}_{}_{}__{}'.format(data.year, data.month, data.day, data.hour, data.minute, data.second, contextDic['newOrOld'])
        contextDic['dataString'] = dataString
    else:
        dataString = contextDic['dataString']
    for device in deviceList:
        myCmdTupleList = [t for t in cmdTupleList if t[0]==device]
        mybridge = bridge.Bridge(device, **contextDic['credentials'])
        cmdStringList_device = sorted(list(set([t[-1] for t in myCmdTupleList])))
        outputCmdStringList_device = mybridge.sendCmdList(cmdStringList_device)
        cmdStringDic = {k:v for k,v in zip(cmdStringList_device, outputCmdStringList_device)}
        dstDir = os.path.join(contextDic['partialDstDir'], dataString)
        #dataCollection(mybridge, cmdStringList_device, dstDir)
        for cmdTuple in myCmdTupleList:
            logicalIf = cmdTuple[1]
            cmd = cmdTuple[-1]
            cmdOut = cmdStringDic[cmd]
            print('saving - {}\t{}'.format(device, cmd))
            file_name = os.path.join(dstDir, device, dataCollection_editName(logicalIf))
            os.makedirs(file_name, exist_ok=True)
            newName = dataCollection_editName(cmd)
            with open(os.path.join(file_name, newName + '.txt'), 'w') as f:
                f.write(cmdOut)
        ### print bgp routes ###
        device_routes = [t[-1] for t in cmdTupleList_bgp_routes if t[0]==device]
        if device_routes:
            with open(os.path.join(dstDir, device)+'_routes.txt', 'w') as f:
                f.write('\n'.join(device_routes))

    return physicalParse, serviceParse, dataString

def runDataMining(contextDic):
    physicalParse = contextDic['physicalParse']
    serviceParse = contextDic['serviceParse']
    partialDstDir = contextDic['partialDstDir']
    dataString = contextDic['dataString']
    workbook = contextDic['workbook']
    ########################################### DATA MINING ###########################################
    physicalParse.dataMining(logDir=partialDstDir, dataString=dataString, workbook=workbook)
    serviceParse.dataMining(logDir=partialDstDir, dataString=dataString, workbook=workbook)

def runGetDataMining(contextDic, phy=True):
    physicalParse = contextDic['physicalParse']
    serviceParse = contextDic['serviceParse']
    partialDstDir = contextDic['partialDstDir']
    dataString = contextDic['dataString']
    workbook = contextDic['workbook']
    ########################################### DATA MINING ###########################################
    if phy:
        physicalParse.dataMining(logDir=partialDstDir, dataString=dataString, workbook=workbook)
    serviceParse.getMining(logDir=partialDstDir, dataString=dataString)

def runGetAll(contextDic_OLD, contextDic_NEW):
    partialDstDir = contextDic_OLD['partialDstDir']
    workbook = contextDic_OLD['workbook']
    runDataCollection(contextDic_OLD)
    dataString = contextDic_OLD['dataString']
    runGetDataMining(contextDic_OLD)
    contextDic_OLD['serviceParse'].dataAll(logDir=partialDstDir, dataString=dataString, workbook=workbook)
    #
    runDataCollection(contextDic_NEW, ['[EXTRA]','[PHY]','[BRIDGE]'])
    runGetDataMining(contextDic_NEW)
    #
    ### create sheet L2VPN ###
    createSheetL2VPN(contextDic_OLD, contextDic_NEW)

def createSheetL2VPN(contextDic_OLD, contextDic_NEW):
    old_serviceParse = contextDic_OLD['serviceParse']
    new_serviceParse = contextDic_NEW['serviceParse']
    workbook = contextDic_OLD['workbook']
    mysheet = workbook.create_sheet('PRE__' )#+ contextDic_OLD['dataString'])
    keys_list = serv.Service.getServiceMiningDic_SortedKeys_bridgeLimited_split()
    l = printableInXlsList(keys_list[0] + keys_list[1] + keys_list[1])
    mysheet.append(l)
    print('\n\n')
    s = ('{:30.30}|'*len(l)).format(*l)
    print(s)
    print(('-'*30 + '|')*len(l))
    for old_obj, new_obj in zip(old_serviceParse.serviceObjDic.values(), new_serviceParse.serviceObjDic.values()):
        if old_obj.attributeDic.get(serv.Service.BRIDGE_DOMAIN_NAME):
            x = copy.deepcopy(old_obj.serviceMiningDic)
            x.update(old_obj.attributeDic)
            l = printableInXlsList(
                    dic2list(x, keys_list[0] + keys_list[1]) +
                    dic2list(new_obj.serviceMiningDic, keys_list[1]))
            mysheet.append(l)
            #
            ll = []
            for x in l:
                if not x:
                    x = ''
                ll.append(x)
            s = ('{:30.30}|'*len(l)).format(*ll)
            print(s)


def getSheetL2VPN(contextDic_OLD, contextDic_NEW):
    runDataCollection(contextDic_OLD, ['[BRIDGE]'])
    runGetDataMining(contextDic_OLD, phy=False)
    runDataCollection(contextDic_NEW, ['[BRIDGE]'])
    runGetDataMining(contextDic_NEW, phy=False)
    ### create sheet L2VPN ###
    createSheetL2VPN(contextDic_OLD, contextDic_NEW)

def runCompare(oldContextDic, newContextDic):
    old_serviceParse = oldContextDic['serviceParse']
    old_partialDstDir = oldContextDic['partialDstDir']
    old_dataString = oldContextDic['dataString']
    old_serviceParse.getMining(logDir=old_partialDstDir, dataString=old_dataString)
    #
    new_serviceParse = newContextDic['serviceParse']
    new_partialDstDir = newContextDic['partialDstDir']
    new_dataString = newContextDic['dataString']
    new_serviceParse.getMining(logDir=new_partialDstDir, dataString=new_dataString)
    #
    old_serviceParse.cmp(new_serviceParse, wb)

def saveWorkBook(workbook, fileDir):
    directory, oldFileName = os.path.split(fileDir)
    newFileName = input('Save as [{}]:'.format(oldFileName)).strip()
    try:
        if not newFileName:
            #newFileName = oldFileName
            raise
        if not re.search('\.xlsx$', newFileName):
            newFileName = newFileName + '.xlsx'
        workbook.save(os.path.join(directory, newFileName))
        print('Workbook saved with name {}'.format(newFileName))
    except:
        if not newFileName:
            print('Please, write a name for this file!')
        else:
            print('WorkBook with fileName {} is still open, please close it'.format(fileName))


def getBgpVrfScalesheet(beforeDir, afterDir):
    ##
    contextDic_old_before = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
    contextDic_old_before['dataString'] = beforeDir
    serviceParse_old_before = contextDic_old_before['serviceParse']
    serviceParse_old_before.getMining(logDir=partialDstDir, dataString=beforeDir)
    keys, old_before_dic = serviceParse_old_before.readBGPvrfScale(os.path.join(partialDstDir, beforeDir))
    ##
    contextDic_new_before = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
    contextDic_new_before['dataString'] = beforeDir
    serviceParse_new_before = contextDic_new_before['serviceParse']
    serviceParse_new_before.getMining(logDir=partialDstDir, dataString=beforeDir)
    new_before_dic = serviceParse_new_before.readBGPvrfScale(os.path.join(partialDstDir, beforeDir))[1]
    ###
    contextDic_old_after = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
    contextDic_old_after['dataString'] = afterDir
    serviceParse_old_after = contextDic_old_after['serviceParse']
    serviceParse_old_after.getMining(logDir=partialDstDir, dataString=afterDir)
    old_after_dic = serviceParse_old_after.readBGPvrfScale(os.path.join(partialDstDir, afterDir))[1]
    ##
    contextDic_new_after = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
    contextDic_new_after['dataString'] = afterDir
    serviceParse_new_after = contextDic_new_after['serviceParse']
    serviceParse_new_after.getMining(logDir=partialDstDir, dataString=afterDir)
    new_after_dic = serviceParse_new_after.readBGPvrfScale(os.path.join(partialDstDir, afterDir))[1]
    ####
    matrix_list = []
    l = [keys[0], 'B/A'] + keys[1:]
    print(('{}|' * len(l)).format(*l))
    matrix_list.append(l)
    #
    for k_old_before, k_old_after, k_new_before, k_new_after in \
            zip(
                sorted(list(old_before_dic.keys())),
                sorted(list(old_after_dic.keys())),
                sorted(list(new_before_dic.keys())),
                sorted(list(new_after_dic.keys())),
            ):
        old_device_before = old_before_dic[k_old_before]
        old_device_after = old_after_dic[k_old_after]
        new_device_before = new_before_dic[k_new_before]
        new_device_after = new_after_dic[k_new_after]
        len_vrf = len(old_device_before.values())
        print('-MIB', k_old_before, k_old_after, k_new_before, k_new_after, len_vrf)
        for vrf in sorted(list(old_device_before.keys())):
            l = dic2list(old_device_before[vrf], keys=keys)
            l = [l[0]] + ['BEFORE'] + l[1:]
            l = [str(x) for x in l]
            print(('{}|' * len(l)).format(*l))
            matrix_list.append(l)
            old_before_list = l
            #
            l = dic2list(old_device_after[vrf], keys=keys)
            l = [l[0]] + ['AFTER'] + l[1:]
            l = [str(x) for x in l]
            print(('{}|' * len(l)).format(*l))
            matrix_list.append(l)
            old_after_list = l
            #
            l = dic2list(new_device_before[vrf], keys=keys)
            l = [l[0]] + ['BEFORE'] + l[1:]
            l = [str(x) for x in l]
            print(('{}|' * len(l)).format(*l))
            matrix_list.append(l)
            new_before_list = l
            #
            l = dic2list(new_device_after[vrf], keys=keys)
            l = [l[0]] + ['AFTER'] + l[1:]
            l = [str(x) for x in l]
            print(('{}|' * len(l)).format(*l))
            matrix_list.append(l)
            new_after_list = l
            #
            compare_list = []
            for ob, oa, nb, na in zip(old_before_list, old_after_list, new_before_list, new_after_list):
                try:
                    ob = int(ob)
                    oa = int(oa)
                    nb = int(nb)
                    na = int(na)
                except:
                    continue
                compare_list.append(str((oa - ob) + (na - nb) == 0))
            matrix_list.append(['#', 'cmp_BgpScale', vrf] + compare_list)
            #
            print(('-' * 15 + '|') * len(l))
    ##
    print('- print matrix -')
    worksheet = wb.create_sheet('cmd_vrf')
    new_matrix = []
    for i in matrix_list:
        print(('{:15.15}' * len(i)).format(*i))
        worksheet.append(i)

def cleanWorkbook(workbook):
    for sheetname in workbook.sheetnames:
        #TODO:sistemare!
        if 'Physical' in sheetname or sheetname=='Services' or sheetname=='StaticRoutes':
            continue
        worksheet = workbook[sheetname]
        workbook.remove(worksheet)
    return workbook


if __name__ == '__main__':
    logDir = os.path.join(os.getcwd(), 'LOG')
    jsonFile = 'credentials.json'
    for f in [xl for xl in os.listdir(logDir) if re.search('^\w.*\.xlsx', xl)]:
        print(f)
    fileName = input('Select one: ')
    fileDir = os.path.join(logDir, fileName)
    with open(jsonFile) as f:
        credentials = json.load(f)
    wb = openpyxl.load_workbook(fileDir)
    wb = cleanWorkbook(wb)
    ###################################################################################################################
    choice = ''
    while choice != 'exit':
        print('"l2" - phy and pw check')
        print('"0" - Preliminary CheckUP')
        print('"1" - Data Collection on OLD')
        print('"2" - Data Collection on NEW')
        print('"3" - Compare')
        print('"4" - Save Workbook')
        print('"exit" - to quit')
        choice = input('Choice one: ')
        if choice == 'l2':
            partialDstDir = os.path.join(logDir, fileName.split('.')[0])
            contextDic_OLD = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
            contextDic_NEW = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
            #
            data = datetime.datetime.now()
            dataString = '{}_{}_{}__{}_{}_{}__{}'.format(data.year, data.month, data.day, data.hour, data.minute, data.second, __OLD__)
            contextDic_OLD['dataString'] = dataString
            contextDic_NEW['dataString'] = dataString
            #
            getSheetL2VPN(contextDic_OLD, contextDic_NEW)
        elif choice == '0':
            partialDstDir = os.path.join(logDir, fileName.split('.')[0])
            contextDic_OLD = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
            contextDic_NEW = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
            #
            data = datetime.datetime.now()
            dataString = '{}_{}_{}__{}_{}_{}__{}'.format(data.year, data.month, data.day, data.hour, data.minute, data.second, __OLD__)
            contextDic_OLD['dataString'] = dataString
            contextDic_NEW['dataString'] = dataString
            #
            runGetAll(contextDic_OLD, contextDic_NEW)
        elif choice == '1':
            __newOrOld__ = __OLD__
            partialDstDir = os.path.join(logDir, fileName.split('.')[0])
            contextDic = getContextDic(wb, __newOrOld__, partialDstDir, credentials, fileDir)
            #contextDic['dataString'] = '2019_9_2__15_35_7__onOLD' #CT_FAKE
            #contextDic['dataString'] = '2019_9_2__18_21_15__onOLD' #MI_FAKE
            runDataCollection(contextDic)
            runDataMining(contextDic)
            #
            new_contextDic = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
            new_contextDic['dataString'] = contextDic['dataString']
            runDataCollection(new_contextDic, classTagList=['[EXTRA]'])
        elif choice == '2':
            __newOrOld__ = __NEW__
            partialDstDir = os.path.join(logDir, fileName.split('.')[0])
            contextDic = getContextDic(wb, __newOrOld__, partialDstDir, credentials, fileDir)
            #contextDic['dataString'] = '2019_9_2__15_37_12__onNEW' #CT_FAKE
            #contextDic['dataString'] = '2019_9_2__15_37_12__onNEW' #MI_FAKE
            runDataCollection(contextDic)
            runDataMining(contextDic)
            #
            old_contextDic = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
            old_contextDic['dataString'] = contextDic['dataString']
            runDataCollection(old_contextDic, classTagList=['[EXTRA]'])
        elif choice == '3':
            partialDstDir = os.path.join(logDir, fileName.split('.')[0])
            for f in [d for d in os.listdir(partialDstDir) if '.' not in d]:
                print(f)
            choice2Dirs = input('Choice two separated by space (<OLD>+" "+<NEW>) : ')
            oldDir = choice2Dirs.split()[0]
            newDir = choice2Dirs.split()[-1]
            oldContextDic = getContextDic(wb, __OLD__, partialDstDir, credentials, fileDir)
            oldContextDic['dataString'] = oldDir
            newContextDic = getContextDic(wb, __NEW__, partialDstDir, credentials, fileDir)
            newContextDic['dataString'] = newDir
            runCompare(oldContextDic, newContextDic)
            getBgpVrfScalesheet(oldDir, newDir)
        elif choice == '4':
            saveWorkBook(wb, fileDir)